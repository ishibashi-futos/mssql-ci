# MS SQL Example with Gitlab CI

To run locally:

- `docker pull microsoft/mssql-server-linux && docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong(!)Password' -p 1433:1433 -d microsoft/mssql-server-linux:latest`
- copy .env.example to .env

To test on gitlab: just push to a new gitlab repository
