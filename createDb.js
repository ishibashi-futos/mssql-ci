const { Connection, Request } = require('tedious')
const dotenv = require('dotenv')
dotenv.config()

const config = {
  server: process.env.MSSQL_HOST,
  authentication: {
    type: 'default',
    options: {
      userName: 'sa',
      password: 'yourStrong(!)Password',
    },
  },
}

console.log(config)

const connection = new Connection(config)

new Promise((resolve, reject) => {
  connection.on('connect', function(err) {
    connection.execSql(
      new Request('CREATE DATABASE Sales', err => {
        if (err) return reject(err)
        resolve()
      })
    )
  })
})
  .then(() => {
    console.log('Successfully created database')
    process.exit()
  })
  .catch(console.error)
